<?php

namespace Ayzy\IM\Handlers;

use Ayzy\IM\Model\KeFu;
use Illuminate\Support\Facades\Cache;

class KeFuHandler
{
    /**
     * @note:分配客服
     * @author: gzh
     * @time: 18/10/12 下午2:58
     */
    public function getKeFu()
    {

        if (self::keFuWhere()->count() == 0) {
            return ['code' => 0,'msg' => '没有客服在线'];
        } else {
//          查找空闲的客服
            $args = func_get_args();
            $kefu = $args && Cache::get($args[0]) ? KeFu::find(Cache::get($args[0])) : self::getfreeKeFu() ;
            if (empty($kefu)) {
//            没有空闲客服，获取每个客服当前咨询任务的开始时间，将这个用户非配给当前咨询任务的开始时间最早的并且后面排队最少的那个客服
                $kefus = self::getLeastKeFu()->toArray();
                foreach ($kefus as $key => $value) {
                    $tmp[$key] = $value['number'];
                }
                array_multisort($tmp, SORT_ASC, $kefus);
                $kefu = $kefus[0];
            }
        }
        
        return $kefu;
    }

    /**
     * @note:空闲客服
     * @author: gzh
     * @time: 18/10/12 下午2:56
     */
    protected static function getfreeKeFu()
    {
        return self::keFuWhere()->where('number',0)->first();
    }

    /**
     * @note:获取人数最少，服务时间最早的客服
     * @author: gzh
     * @time: 18/10/15 上午11:49
     */
    protected static function getLeastKeFu()
    {
        return self::keFuWhere()->orderBy('server','desc')->limit(10)->get();
    }

    /**
     * @return mixed
     * @note:
     * @author: gzh
     * @time: 18/10/15 上午11:49
     */
    public static function addKeFuNumber($data)
    {
        !(Cache::add($data['user'],$data['kefu'],30) && Cache::put($data['kefu'],self::getCacheKeFuArr($data['kefu'],$data['user']),30)) || KeFu::where('id',$data['kefu'])->update(['number'=>$data['num'] + 1, 'server' => time()]);
    }

    /**
     * @param $kefu
     * @return array
     * @note:
     * @author: gzh
     * @time: 18/10/17 下午3:07
     */
    protected static function getCacheKeFuArr($kefu,$user)
    {
        if ($arr = Cache::get($kefu)) {

            array_push($arr,$user);
            return $arr;
        } else {
            return array($user);
        }
    }

    protected static function keFuWhere()
    {
        return KeFu::where('timestamp','>',strtotime(date('Y-m-d',time())));
    }
}

