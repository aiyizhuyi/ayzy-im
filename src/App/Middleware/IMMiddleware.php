<?php

namespace Ayzy\IM\App\Middleware;

use Ayzy\IM\Facades\IM;
use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Config;

class IMMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {
        $imUser  = IM::getUser(\Auth::user()->_id);
        $passwod = Config::get('ayzy.defaultPassword');
        if ( false === $imUser ) IM::publicRegistration(\Auth::user()->_id, $passwod, $nick_name = '中间件创建用户');
        
        return $next($request);
    }
}
