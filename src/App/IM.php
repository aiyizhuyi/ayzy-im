<?php

namespace Ayzy\IM\App;

use Ayzy\IM\Handlers\KeFuHandler;
use Ayzy\IM\Model\Token;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use link1st\Easemob\App\Easemob;
use link1st\Easemob\App\EasemobError;
use link1st\Easemob\App\Http;

class IM extends Easemob
{
    /**
     * 发送文本消息
     *
     * @param array  $users       [接收的对象数组]
     * @param string $target_type [类型]
     * @param string $message     [内容]
     * @param string $send_user   [消息发送者]
     * @param array  $ext         [消息扩展体]
     *
     * @return mixed
     * @throws EasemobError
     */
    public function sendMessageText( $users, $target_type = 'users', $message = "", $send_user = 'admin', $ext = [] )
    {
        if ( !in_array($target_type, $this->target_array) ) {
            throw new EasemobError('target_type 参数错误！');
        }

        $url    = $this->url . 'messages';
        $option = [
                'target_type' => $target_type,
                'target'      => $users,
                'msg'         => [
                        'type' => 'txt',
                        'msg'  => $message
                ],
                'type'        => 'txt',
                'from'        => $send_user
        ];

        // 是否有消息扩展
        if ( !empty($ext) ) {
            $option['ext'] = $ext;
        }
        $access_token = $this->getToken();
        $header []    = 'Authorization: Bearer ' . $access_token;

        return Http::postCurl($url, $option, $header, 'POST');
    }

    /**
     * 删除所有用户
     *
     * @return bool|mixed
     */
    public function delUsers( $limit = 100 )
    {
        $url          = $this->url . 'users?limit=' . $limit;
        $option       = [];
        $access_token = $this->getToken();
        $header []    = 'Authorization: Bearer ' . $access_token;
        try {
            $result = Http::postCurl($url, $option, $header, 'DELETE');

            return $result;
        } catch ( \Exception $e ) {
            return false;
        }
    }

    /**
     * 获取用户状态
     */
    public function getUserStatus( $username )
    {
        $access_token = $this->getToken();
        $header []    = 'Authorization: Bearer ' . $access_token;
        $url          = $this->url . 'users/' . $username . '/status';
        $option       = [];
        try {
            $result = Http::postCurl($url, $option, $header, 'GET');

            return reset($result['data']);
        } catch ( \Exception $e ) {
            return false;
        }
    }

    /**
     * 消息转换
     *
     * @param $message
     * @return array
     */
    public function convertMessage( $message )
    {
        $type[0]  = 'text';
        $value[0] = $message;
        $result   = preg_match_all('/(voice|img|video|face|html)\[(.*?)\]/', $message, $matches);
        if ( !empty($result) ) list($message, $type, $value) = $matches;
        if ( !empty($a) ) list($message, $type, $value) = $matches;
        $type  = reset($type);
        $value = reset($value);
        if ( $type === 'face' ) {
            $type  = 'text';
            $value = preg_replace_callback('/(face)(\[.*?\])/', function( $meatches ) {
                list($a, $b, $c) = $meatches;

                return $c;
            }, $message);
            $value = reset($value);
        }

        return compact('type', 'value');
    }

    /**
     * 上传文件
     *
     * @param $file_path
     *
     * @return mixed
     * @throws EasemobError
     */
    public function uploadFile( $file_path )
    {
        $result = parent::uploadFile($file_path);

        return $result;
    }

    /**
     * 获取用户token
     *
     * @param string $memberIdx
     * @param string $pwd
     * @param array  $data
     * @return mixed
     */
    public function getUserToken( $username = 'erdong2011', $pwd = 'uio234' )
    {
        $token = Token::where('username', $username)->where('timestamp', '<', time() + 604800)->orderBy('timestamp', 'desc')->first();
        if ( !is_null($token) ) {
            return $token->token;
        } else {
            $url    = $this->url . 'token';
            $option = [
                    'grant_type' => 'password',
                    'username'   => $username,
                    'password'   => $pwd,
            ];
            try {
                $return = Http::postCurl($url, $option);
            } catch ( \Exception $e ) {
                return false;
            }

            Token::add(['username' => $username, 'token' => $return['access_token'], 'expires_in' => $return['expires_in']]);

            return $return['access_token'];
        }

        return $user_token;
    }


    /**
     * 注册用户
     *
     * @param        $name
     * @param string $password
     * @param string $nick_name
     * @return bool|mixed
     */
    public function publicRegistration( $name, $password = 'uio234', $nick_name = "" )
    {
        try {
            $im = parent::publicRegistration($name, $password, $nick_name); // TODO: Change the autogenerated stub

            return $im['entities'][0];
        } catch ( \Exception $e ) {
            return false;
        }
    }

    /**
     * 获取用户
     *
     * @param $user_name
     * @return bool|mixed
     */
    public function getUser( $user_name )
    {
        try {
            $im = parent::getUser($user_name); // TODO: Change the autogenerated stub

            return $im['entities'][0];
        } catch ( \Exception $e ) {
            return false;
        }
    }

    /**
     * 获取聊天记录
     *
     * @param $time
     * @return mixed
     * @throws \Exception
     */
    public function getChatMessage( $time = '2018092814' )
    {
        $url          = $this->url . 'chatmessages/' . $time;
        $access_token = $this->getToken();
        $header []    = 'Authorization: Bearer ' . $access_token;
        $header []    = '"Content-Type":"application/json"';
        $option       = [];
        try {
            $result = HttpBase::postCurl($url, $option, $header, 'GET');
            $url    = $this->getRemoteFile($result['data'][0]['url']);
            $data   = $this->getGzFile($url);

            return $data;
        } catch ( \Exception $e ) {
            echo $e->getMessage();
            $result = false;
        }

        return $result;
    }

    /**
     * 获取远程文件
     *
     * @param $url
     * @return mixed
     */
    protected function getRemoteFile( $url )
    {
        set_time_limit(24 * 60 * 60); // 设置超时时间
        $newFileName = basename($url);
        $newFileName = explode('?', $newFileName);
        $newFileName = reset($newFileName);
        $newFileName = 'messages/' . $newFileName;
        if ( $newFileName ) {
            $file = fopen($url, "rb");
            Storage::disk('local')->put($newFileName, $file);
        }

        return storage_path('app/' . $newFileName);
    }

    /**
     * 读取.gz 文件并将内容解析
     *
     * @param string $url
     * @return array
     */
    protected function getGzFile( $url )
    {
        $file     = gzopen($url, 'r');
        $fileSize = filesize($url);
        $str      = '';
        while ( !gzeof($file) ) $str .= gzread($file, $fileSize);
        gzclose($file);
        $message = explode("\n", $str);
        $message = array_filter($message);
        foreach ( $message as &$v ) $v = json_decode($v, true);
        unset($v);

        return $message;
    }

    /**
     * 右侧辅助的html
     *
     * @param $title
     * @param $body
     * @return string
     */
    public function assistHTML( $title, $body )
    {
        return sprintf(
                '<div class="-assist-info">
                            <div class="-assist-info-heading"><i class="icon-arrow"></i> %s</div>
                            <div class="-assist-info-body" style="padding-left: 10px;">%s</div>
                        </div>',
                $title,
                $body
        );
    }

    /**
     * 右侧辅助函数
     */
    public function assistUsername()
    {
        return Input::get('username');
    }

    /**
     * @param KeFuHandler $kefuhandler
     * @return mixed
     * @note  :获取客服
     * @author: gzh
     * @time  : Times
     */
    public function getKeFu()
    {
        $args        = func_get_args();
        $kefuhandler = new KeFuHandler();

        if ( empty($args) ) {
            return $kefuhandler->getKeFu();
        } else {
            return $kefuhandler->getKeFu(...$args);
        }
    }
}