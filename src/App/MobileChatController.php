<?php

namespace Ayzy\IM\App;

use App\Ayzy\Facades\WeChat;
use App\Http\Controllers\Controller;
use Ayzy\IM\Handlers\KeFuHandler;
use Ayzy\IM\Model\Guest;
use Ayzy\IM\Model\KeFu;
use Ayzy\IM\Model\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Ayzy\IM\Facades\IM;
use Illuminate\Support\Facades\Storage;

class MobileChatController extends Controller
{
    private $password;

    public function __construct()
    {
        $this->password = Config::get('ayzy.defaultPassword');
    }

    public function index()
    {
        return view('Ayzy::chat');
    }

    /**
     * 获取配置文件
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function config()
    {
        $token             = IM::getUserToken(Auth::user()->_id, $this->password);
        $argument          = Config::get('ayzy');
        $argument['user']  = Auth::user()->_id;
        $argument['token'] = $token;

        return response()->json($argument);
    }

    /**
     * 登录成功
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function isLogin()
    {
        $username = Input::get('username');
        $kefu     = KeFu::where('id', $username)->first();
        if ( is_null($kefu) ) {
            KeFu::add(['id' => $username, 'number' => 0, 'timestamp' => time()]);
        } else {
            $where['timestamp'] = time();
            ( $kefu->timestamp > strtotime(date('Y-m-d')) ) && $where['number'] = 0;
            KeFu::edit($kefu, $where);
        }

        return response()->json(['status' => 0, 'message' => '成功']);
    }

    /**
     * 保存聊天记录
     */
    public function save()
    {
        $input = Input::get('data');
        if ( isset($input['item']) && $input['item'] === 'wechat' ) {
            $input['from'] = WeChat::asciitostr($input['from']);
        } else {
            $input['to'] = WeChat::asciitostr($input['to']);
        }
        WeChat::push($input['to'], $input['data']);
        $message            = new Message();
        $message->timestamp = time();
        foreach ( $input as $k => $v ) $message->$k = $v;
        $message->is_active = 1;
        $message->save();

        return response()->json(['status' => 0, 'message' => '保存成功', 'data' => []]);
    }

    /**
     * 获取用户
     */
    public function getUser()
    {
        $from    = Auth::user()->_id;
        $message = Message::where('is_active', 1)->where(function( $query ) use ( $from ) {
            $query->where('to', $from)->orwhere('from', $from);
        })->groupBy(['to', 'from'])->orderBy('created_at', 'desc')->get(['to', 'from', 'data', 'timestamp', 'ext']);
        $openId  = [];
        foreach ( $message as $v ) {
            $openId[] = $v->to;
            $openId[] = $v->from;
        }
        $openId    = array_unique(array_filter($openId));
        $guest     = Guest::whereIn('_id', $openId)->get();
        $guestName = [];
        foreach ( $guest as $v ) {
            $guestName[$v->_id] = [
                    'name'   => empty($v->name) ? ( $v->nickname ?? '' ) : $v->name,
                    'avatar' => $v->headimgurl,
                    'tel'    => $v->tel,
            ];
        }
        $data = [];
        foreach ( $message as $v ) {
            if ( strlen($v->to) === 28 ) {
                $openId = $v->to;
            } else {
                $openId = $v->from;
            }
            $id = WeChat::strtoascii($openId);
            if ( !( isset($data[$id]) && $data[$id]['timestamp'] > $v->timestamp ) ) {
                $data[$id] = [
                        'username'  => $id,
                        'nickname'  => $guestName[$openId]['name'],
                        'avatar'    => $guestName[$openId]['avatar'],
                        'tel'       => $guestName[$openId]['tel'],
                        'message'   => $v->data,
                        'timestamp' => $v->timestamp,
                        'ext'       => $v->ext,
                ];
            }
        }

        uasort($data, function( $a, $b ) {
            if ( $a['timestamp'] == $b['timestamp'] ) {
                return 0;
            }

            return ( $a['timestamp'] > $b['timestamp'] ) ? -1 : 1;
        });

        return response()->json(['status' => 0, 'message' => '获取成功', 'data' => $data]);
    }

    /**
     * 获取聊天记录
     */
    public function getMessage()
    {
        $to      = Input::get('to');
        $to      = WeChat::asciitostr($to);
        $from    = Input::get('from');
        $limit   = Input::get('limit') ?? Config::get('ayzy.messageLimit');
        $offset  = Input::get('offset') ?? 0;
        $message = Message::where(function( $query ) use ( $to ) {
            return $query->where('to', $to)->orwhere('from', $to);
        })->where(function( $query ) use ( $from, $to ) {
            return $query->where('from', $from)->orWhere('to', $from);
        })->where('is_active', 1)
                ->orderBy('created_at', 'desc')
                ->limit(intval($limit))
                ->skip(intval($offset))
                ->get()// ['from', 'sourceMsg', 'data', 'to']
                ->toArray();
        foreach ( $message as &$v ) {
            if ( $to === $v['to'] ) {
                $v['from'] = $from;
            }
        }
        $data = array_reverse($message);
        usort($data, function( $a, $b ) {
            if ( $a['timestamp'] == $b['timestamp'] ) return 0;

            return ( $a['timestamp'] < $b['timestamp'] ) ? -1 : 1;
        });

        return response()->json(['status' => 0, 'message' => '获取成功', 'data' => $data]);
    }

    /**
     * 上传文件
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload( Request $request )
    {
        $filename = explode('.', $request->file('file')->getClientOriginalName());
        $ext      = end($filename);
        $timer    = date('Ymd');
        $newName  = $this->random(32, $ext);
        $url      = $timer . DIRECTORY_SEPARATOR . $newName;
        Storage::disk('IM')->put($url, file_get_contents($request->file('file')->getRealPath()));
        $a = 'img[' . Storage::disk('IM')->url('im' . DIRECTORY_SEPARATOR . $url) . ']';

        return response()->json(['status' => 0, 'message' => '上传成功', 'data' => [$a]]);
    }

    /**
     * 获取随机字串文件名
     *
     * @param int $length
     * @param string 扩展名
     * @return string
     */
    public function random( $length = 32, $ext = 'jpg' )
    {
        if ( 'jpeg' === $ext ) $ext = 'jpg';

        return str_random($length) . '.' . $ext;
    }

    /**
     * @param KeFuHandler $kefuhandler
     * @return mixed
     * @note  :修改客服访客数据
     * @author: gzh
     * @time  : Times
     */
    public static function editKeFu()
    {
        $kefuhandler = new KeFuHandler();

        return $kefuhandler->addKeFuNumber(Input::get());
    }
}