<?php

namespace Ayzy\IM\Facades;

use Illuminate\Support\Facades\Facade;

class IM extends Facade
{

    /**
     *
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \Ayzy\IM\App\IM::class;
    }
}
