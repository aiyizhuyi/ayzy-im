<?php

namespace Ayzy\IM;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

use Ayzy\IM\App\IM;

class IMServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;


    /**
     * 引导程序
     *
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
                __DIR__ . '/config/ayzy.php' => config_path('ayzy.php'),
                __DIR__ . '/Resources'       => public_path('assets/plugins'),
        ]);
    }

    /**
     * Load the given routes file if routes are not already cached.
     *
     * @param  string $path
     * @return void
     */
    protected function loadRoutesFrom( $path )
    {
        if ( !$this->app->routesAreCached() ) {
            require $path;
        }
    }

    /**
     * 默认包位置
     *
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
//        // 将数据库配置文件与系统配置文件接合
        $this->mergeConfigFrom(
                __DIR__ . '/config/database.php', 'database.connections'
        );
        // 将文件系统配置文件与系统配置文件接合
        $this->mergeConfigFrom(
                __DIR__ . '/config/filesystems.php', 'filesystems.disks'
        );
        // 加载路由
        $this->loadRoutesFrom(
                __DIR__ . '/Route/routes.php'
        );
        // 加载视图
        $this->loadViewsFrom(
                __DIR__ . '/View',
                'Ayzy'
        );

        // 容器绑定
        $this->app->bind('IM', function() {
            return new IM();
        });


    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

}
