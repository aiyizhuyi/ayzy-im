<a class="closedbar inner hidden-sm hidden-xs" href="#"></a>
<nav id="pageslide-left" class="pageslide inner">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <div class="main-navigation left-wrapper transition-left">
            <div class="navigation-toggler hidden-sm hidden-xs">
                <a href="#main-navbar" class="sb-toggle-left">
                </a>
            </div>
            <div class="user-profile border-top padding-horizontal-10 block">
                <div class="inline-block">
                    <img src="{{ Auth::user()->headimg ?? '/assets/images/avatar-1-small.jpg' }}" alt="">
                </div>
                <div class="inline-block">
                    <h5 class="no-margin"> 欢迎 </h5>
                    <h4 class="no-margin"> {{ Auth::user()->name ?? '' }} </h4>
                    {{--<a class="btn user-options sb_toggle">--}}
                    {{--<i class="fa fa-cog"></i>--}}
                    {{--</a>--}}
                </div>
            </div>
            <!-- start: MAIN NAVIGATION MENU -->
            <ul class="main-navigation-menu">
                <li class="">
                    <a href="/">
                        <i class="fa fa-home"></i>
                        <span class="title">首页 </span>
                        {{--<span class="label label-default pull-right ">LABEL</span>--}}
                    </a>
                </li>
                <li class="">
                    <a href="/admin/user">
                        <i class="fa fa-home"></i>
                        用户管理
                    </a>
                </li>
                <li class="">
                    <a href="/admin/guest">
                        <i class="fa fa-home"></i>
                        微信用户
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                        <i class="fa fa-folder-open"></i>
                        <span class="title"> 4 Level Menu </span>
                        <i class="icon-arrow"></i>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="javascript:;">
                                Item 1
                                <i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="javascript:;">
                                        Sample Link 1
                                        <i class="icon-arrow"></i>
                                    </a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-times"></i>
                                                Sample Link 1
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-pencil"></i>
                                                Sample Link 1
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-edit"></i>
                                                Sample Link 1
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        Sample Link 1
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Sample Link 2
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Sample Link 3
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;">
                                Item 2
                                <i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="#">
                                        Sample Link 1
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Sample Link 1
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Sample Link 1
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                Item 3
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- end: MAIN NAVIGATION MENU -->
        </div>
        <!-- end: SIDEBAR -->
    </div>
    <div class="slide-tools">
        <div class="col-xs-6 text-left no-padding">
            <a class="btn btn-sm status" href="#">
                状态
                <i class="fa fa-dot-circle-o text-green"></i>
                <span>在线</span>
            </a>
        </div>
        <div class="col-xs-6 text-right no-padding">
            <a class="btn btn-sm log-out text-right" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-power-off"></i>
                退出
            </a>
        </div>
    </div>
</nav>
