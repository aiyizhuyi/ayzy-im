<header class="topbar navbar navbar-inverse navbar-fixed-top inner">
    <!-- start: TOPBAR CONTAINER -->
    <div class="container">
        <div class="navbar-header">
            <a class="sb-toggle-left hidden-md hidden-lg" href="#main-navbar">
                <i class="fa fa-bars"></i>
            </a>
            <!-- start: LOGO -->
            <a class="navbar-brand" href="/">
                <img src="/assets/images/logo.png" alt="{{ config('app.name') }}" />
            </a>
            <!-- end: LOGO -->
        </div>
        <div class="topbar-tools">
            <!-- start: TOP NAVIGATION MENU -->
            <ul class="nav navbar-right">
                <!-- start: USER DROPDOWN -->
                <li class="dropdown current-user">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                        <img src="{{ Auth::user()->headimg ?? '/assets/images/avatar-1-small.jpg' }}" class="img-circle" alt="">
                        <span class="username hidden-xs">{{ Auth::user()->name ?? '' }}</span>
                        <i class="fa fa-caret-down "></i>
                    </a>
                    <ul class="dropdown-menu dropdown-dark">
                        <li>
                            <a href="/">
                                My Profile
                            </a>
                        </li>
                        <li>
                            <a href="/">
                                My Calendar
                            </a>
                        </li>
                        <li>
                            <a href="/">
                                My Messages (3)
                            </a>
                        </li>
                        <li>
                            <a href="/">
                                Lock Screen
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                退出
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- end: USER DROPDOWN -->
                <li class="right-menu-toggle">
                    <a href="#" class="sb-toggle-right">
                        <i class="fa fa-globe toggle-icon"></i>
                        <i class="fa fa-caret-right"></i>
                        <span class="notifications-count badge badge-default"> 3</span>
                    </a>
                </li>
            </ul>
            <!-- end: TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- end: TOPBAR CONTAINER -->
</header>