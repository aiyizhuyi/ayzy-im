<!DOCTYPE html>
<!-- template name: rapido - responsive admin template build with twitter bootstrap 3.x version: 1.2 author: cliptheme -->
<!--[if IE 8]>
<html class="ie8" lang="{{ app()->getLocale() }}"><![endif]-->
<!--[if IE 9]>
<html class="ie9" lang="{{ app()->getLocale() }}"><![endif]-->
<!--[if !IE]><!-->
<html lang="{{ app()->getLocale() }}">
<!--<![endif]-->
<!-- start: head -->
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- start: meta -->
    <meta charset="utf-8" />
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- end: meta -->
    <!-- start: main css -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/animate.css/animate.min.css">
    <!-- end: main css -->
    <!-- start: css required for subview contents -->
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/owl-carousel/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/owl-carousel/owl-carousel/owl.transitions.css">
{{--<link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/summernote/dist/summernote.css">--}}
<!-- 日历 -->
    {{--<link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/fullcalendar/fullcalendar/fullcalendar.css">--}}
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/toastr/toastr.min.css">
    {{--<link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-select/bootstrap-select.min.css">--}}
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    {{--<link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/DataTables/media/css/DT_bootstrap.css">--}}
    {{--<link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">--}}
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-table/dist/bootstrap-table.css">
    <!-- end: css required for this subview contents-->
    <!-- start: css required for this page only -->
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/weather-icons/css/weather-icons.min.css">
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/nvd3/nv.d3.min.css">
    <!-- end: css required for this page only -->
    <link rel="stylesheet" href="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/sweetalert/lib/sweet-alert.css" />
    <!-- start: core css -->
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link rel="stylesheet" href="/assets/css/styles-responsive.css">
    <link rel="stylesheet" href="/assets/css/plugins.css">
    <link rel="stylesheet" href="/assets/css/themes/theme-style8.css" type="text/css" id="skin_color">
    <link rel="stylesheet" href="/assets/css/print.css" type="text/css" media="print" />
    <!-- end: core css -->
    <link rel="shortcut icon" href="/favicon.ico" />
    <style>
        table > thead > tr > th {
            text-align: center !important;
        }
    </style>
    @yield('head')
</head>
<!-- end: head -->
<!-- start: body -->
<body>
<!-- start: sliding bar (sb) -->
@include('Ayzy::layouts.slidingbar-area')

<!-- end: sliding bar -->
<div class="main-wrapper">
    <!-- start: topbar -->
@include('Ayzy::layouts.header')
<!-- end: topbar -->
    <!-- start: pageslide left -->@include('Ayzy::layouts.pageslide-left')<!-- end: pageslide left -->
    <!-- start: pageslide right -->@include('Ayzy::layouts.pageslide-right')<!-- end: pageslide right -->
    <!-- start: main container -->@include('Ayzy::layouts.main-container')<!-- end: main container -->
    <!-- start: footer -->@include('Ayzy::layouts.footer')<!-- end: footer -->
    <!-- start: subview sample contents -->@yield('subview')<!-- end: subview sample contents -->
</div>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<!-- start: main javascripts -->
<!--[if lt IE 9]>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/respond.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/excanvas.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery/jquery-1.11.1.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jQuery/jquery-2.1.1.min.js"></script>
<!--<![endif]-->
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/blockUI/jquery.blockUI.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/moment/min/moment.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootbox/bootbox.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery.appear/jquery.appear.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/velocity/jquery.velocity.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-table/bootstrap-table-zh-CN.js"></script>
<!-- end: main javascripts -->
<!-- start: javascripts required for subview contents -->
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
{{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery-mockjax/jquery.mockjax.js"></script>--}}
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/toastr/toastr.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<!-- 日历 -->
{{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>--}}
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
{{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-select/bootstrap-select.min.js"></script>--}}
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
{{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>--}}
{{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>--}}

<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/truncate/jquery.truncate.js"></script>
{{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/summernote/dist/summernote.min.js"></script>--}}
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/assets/js/subview.js"></script>
{{--<script src="/assets/js/subview-examples.js"></script>--}}
<!-- end: javascripts required for subview contents -->
<!-- start: javascripts required for this page only -->
{{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>--}}
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/nvd3/lib/d3.v3.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/nvd3/nv.d3.min.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/nvd3/src/models/historicalBar.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/nvd3/src/models/historicalBarChart.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/nvd3/src/models/stackedArea.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/nvd3/src/models/stackedAreaChart.js"></script>
{{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/jquery.sparkline/jquery.sparkline.js"></script>--}}
{{--<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/easy-pie-chart/dist/jquery.easypiechart.min.js"></script>--}}
<script src="http://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/sweetalert/lib/sweet-alert.min.js"></script>
{{--<script src="/assets/js/index.js"></script>--}}
<!-- end: javascripts required for this page only -->
<!-- start: core javascripts  -->
<script src="{{ asset('/assets/js/main.js') }}"></script>
<!-- end: core javascripts  -->
@yield('script')
</body>
<!-- end: body -->
</html>