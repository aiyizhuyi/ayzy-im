<div id="pageslide-right" class="pageslide slide-fixed inner">
    <div class="right-wrapper">
        <ul class="nav nav-tabs nav-justified" id="sidebar-tab">
            <li class="active">
                <a href="#users" role="tab" data-toggle="tab">
                    <i class="fa fa-users"></i>
                </a>
            </li>
            <li>
                <a href="#notifications" role="tab" data-toggle="tab">
                    <i class="fa fa-bookmark "></i>
                </a>
            </li>
            <li>
                <a href="#settings" role="tab" data-toggle="tab">
                    <i class="fa fa-gear"></i>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="users">
                <div class="users-list">
                    <ul class="media-list">
                        <li class="media">
                            <a href="#">
                                <i class="fa fa-circle status-online"></i>
                                <img alt="..." src="/assets/images/avatar-2.jpg" class="media-object">
                                <div class="media-body">
                                    <h4 class="media-heading">Nicole Bell</h4>
                                    <span> Content Designer </span>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="#">
                                <div class="user-label">
                                    <span class="label label-default">3</span>
                                </div>
                                <i class="fa fa-circle status-online"></i>
                                <img alt="..." src="/assets/images/avatar-3.jpg" class="media-object">
                                <div class="media-body">
                                    <h4 class="media-heading">Steven Thompson</h4>
                                    <span> Visual Designer </span>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="#">
                                <i class="fa fa-circle status-online"></i>
                                <img alt="..." src="/assets/images/avatar-4.jpg" class="media-object">
                                <div class="media-body">
                                    <h4 class="media-heading">Ella Patterson</h4>
                                    <span> Web Editor </span>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="#">
                                <i class="fa fa-circle status-online"></i>
                                <img alt="..." src="/assets/images/avatar-5.jpg" class="media-object">
                                <div class="media-body">
                                    <h4 class="media-heading">Kenneth Ross</h4>
                                    <span> Senior Designer </span>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="user-chat">
                    <div class="sidebar-content">
                        <a class="sidebar-back" href="#">
                            <i class="fa fa-chevron-circle-left"></i>
                            返回
                        </a>
                    </div>
                    <ol class="discussion sidebar-content">
                        <li class="other">
                            <div class="avatar">
                                <img src="/assets/images/avatar-4.jpg" alt="">
                            </div>
                            <div class="messages">
                                <p>
                                    上善若水
                                </p>
                                <span class="time"> 51分钟以前 </span>
                            </div>
                        </li>
                        <li class="self">
                            <div class="avatar">
                                <img src="/assets/images/avatar-1.jpg" alt="">
                            </div>
                            <div class="messages">
                                <p>
                                    水善利万物而不争
                                </p>
                                <span class="time"> 37分钟以前 </span>
                            </div>
                        </li>
                        <li class="other">
                            <div class="avatar">
                                <img src="/assets/images/avatar-4.jpg" alt="">
                            </div>
                            <div class="messages">
                                <p>
                                    处万人之所恶
                                </p>
                            </div>
                        </li></li>
                        <li class="self">
                            <div class="avatar">
                                <img src="/assets/images/avatar-1.jpg" alt="">
                            </div>
                            <div class="messages">
                                <p>
                                    故几于道
                                </p>
                                <span class="time"> 37 mins </span>
                            </div>
                        </li>
                        <li class="other">
                            <div class="avatar">
                                <img src="/assets/images/avatar-4.jpg" alt="">
                            </div>
                            <div class="messages">
                                <p>
                                    居善地
                                </p>
                            </div>
                        </li></li>
                        <li class="self">
                            <div class="avatar">
                                <img src="/assets/images/avatar-1.jpg" alt="">
                            </div>
                            <div class="messages">
                                <p>
                                   心善渊
                                </p>
                                <span class="time"> 37 mins </span>
                            </div>
                        </li>
                        <li class="other">
                            <div class="avatar">
                                <img src="/assets/images/avatar-4.jpg" alt="">
                            </div>
                            <div class="messages">
                                <p>
                                   尹善仁
                                </p>
                            </div>
                        </li></li>
                        <li class="self">
                            <div class="avatar">
                                <img src="/assets/images/avatar-1.jpg" alt="">
                            </div>
                            <div class="messages">
                                <p>
                                    言善信
                                </p>
                                <span class="time"> 37 mins </span>
                            </div>
                        </li>
                        <li class="other">
                            <div class="avatar">
                                <img src="/assets/images/avatar-4.jpg" alt="">
                            </div>
                            <div class="messages">
                                <p>
                                    夫唯不争
                                </p>
                            </div>
                        </li>
                        <li class="self">
                            <div class="avatar">
                                <img src="/assets/images/avatar-1.jpg" alt="">
                            </div>
                            <div class="messages">
                                <p>
                                    故无由
                                </p>
                                <span class="time"> 37 mins </span>
                            </div>
                        </li>
                    </ol>
                    <div class="user-chat-form sidebar-content">
                        <div class="input-group">
                            <input type="text" placeholder="Type a message here..." class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-blue no-radius" type="button">
                                    <i class="fa fa-chevron-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="notifications">
                {{--<div class="notifications">--}}
                    {{--<div class="pageslide-title">--}}
                        {{--You have 11 notifications--}}
                    {{--</div>--}}
                    {{--<ul class="pageslide-list">--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)">--}}
                                {{--<span class="label label-primary"><i class="fa fa-user"></i></span>--}}
                                {{--<span class="message"> New user registration</span>--}}
                                {{--<span class="time"> 1 min</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)">--}}
                                {{--<span class="label label-success"><i class="fa fa-comment"></i></span>--}}
                                {{--<span class="message"> New comment</span>--}}
                                {{--<span class="time"> 7 min</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)">--}}
                                {{--<span class="label label-success"><i class="fa fa-comment"></i></span>--}}
                                {{--<span class="message"> New comment</span>--}}
                                {{--<span class="time"> 8 min</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)">--}}
                                {{--<span class="label label-success"><i class="fa fa-comment"></i></span>--}}
                                {{--<span class="message"> New comment</span>--}}
                                {{--<span class="time"> 16 min</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)">--}}
                                {{--<span class="label label-primary"><i class="fa fa-user"></i></span>--}}
                                {{--<span class="message"> New user registration</span>--}}
                                {{--<span class="time"> 36 min</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:void(0)">--}}
                                {{--<span class="label label-warning"><i class="fa fa-shopping-cart"></i></span>--}}
                                {{--<span class="message"> 2 items sold</span>--}}
                                {{--<span class="time"> 1 hour</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="warning">--}}
                            {{--<a href="javascript:void(0)">--}}
                                {{--<span class="label label-danger"><i class="fa fa-user"></i></span>--}}
                                {{--<span class="message"> User deleted account</span>--}}
                                {{--<span class="time"> 2 hour</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                    {{--<div class="view-all">--}}
                        {{--<a href="javascript:void(0)">--}}
                            {{--See all notifications--}}
                            {{--<i class="fa fa-arrow-circle-o-right"></i>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="tab-pane" id="settings">
                {{--<h5 class="sidebar-title">General Settings</h5>--}}
                {{--<ul class="media-list">--}}
                    {{--<li class="media">--}}
                        {{--<div class="checkbox sidebar-content">--}}
                            {{--<label>--}}
                                {{--<input type="checkbox" value="" class="green" checked="checked">--}}
                                {{--Enable Notifications--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="media">--}}
                        {{--<div class="checkbox sidebar-content">--}}
                            {{--<label>--}}
                                {{--<input type="checkbox" value="" class="green" checked="checked">--}}
                                {{--Show your E-mail--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="media">--}}
                        {{--<div class="checkbox sidebar-content">--}}
                            {{--<label>--}}
                                {{--<input type="checkbox" value="" class="green">--}}
                                {{--Show Offline Users--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="media">--}}
                        {{--<div class="checkbox sidebar-content">--}}
                            {{--<label>--}}
                                {{--<input type="checkbox" value="" class="green" checked="checked">--}}
                                {{--E-mail Alerts--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="media">--}}
                        {{--<div class="checkbox sidebar-content">--}}
                            {{--<label>--}}
                                {{--<input type="checkbox" value="" class="green">--}}
                                {{--SMS Alerts--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                {{--</ul>--}}
                {{--<div class="sidebar-content">--}}
                    {{--<button class="btn btn-success">--}}
                        {{--<i class="icon-settings"></i>--}}
                        {{--Save Changes--}}
                    {{--</button>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="hidden-xs" id="style_selector">
            <div id="style_selector_container">
                <div class="pageslide-title">
                    样式选择
                </div>
                <div class="box-title">
                    选择你的布局样式
                </div>
                <div class="input-box">
                    <div class="input">
                        <select name="layout" class="form-control">
                            <option value="default">默认</option>
                            <option value="boxed">盒子型</option>
                        </select>
                    </div>
                </div>
                <div class="box-title">
                    选择你的头部样式
                </div>
                <div class="input-box">
                    <div class="input">
                        <select name="header" class="form-control">
                            <option value="fixed">固定</option>
                            <option value="default">可活动</option>
                        </select>
                    </div>
                </div>
                <div class="box-title">
                    选择你的侧边栏样式
                </div>
                <div class="input-box">
                    <div class="input">
                        <select name="sidebar" class="form-control">
                            <option value="fixed">固定</option>
                            <option value="default">可活动</option>
                        </select>
                    </div>
                </div>
                <div class="box-title">
                    选择你的尾部样式
                </div>
                <div class="input-box">
                    <div class="input">
                        <select name="footer" class="form-control">
                            <option value="default">可活动</option>
                            <option value="fixed">固定</option>
                        </select>
                    </div>
                </div>
                <div class="box-title">
                    10 种预定义颜色方案
                </div>
                <div class="images icons-color">
                    <a href="#" id="default">
                        <img src="/assets/images/color-1.png" alt="" class="active">
                    </a>
                    <a href="#" id="style2">
                        <img src="/assets/images/color-2.png" alt="">
                    </a>
                    <a href="#" id="style3">
                        <img src="/assets/images/color-3.png" alt="">
                    </a>
                    <a href="#" id="style4">
                        <img src="/assets/images/color-4.png" alt="">
                    </a>
                    <a href="#" id="style5">
                        <img src="/assets/images/color-5.png" alt="">
                    </a>
                    <a href="#" id="style6">
                        <img src="/assets/images/color-6.png" alt="">
                    </a>
                    <a href="#" id="style7">
                        <img src="/assets/images/color-7.png" alt="">
                    </a>
                    <a href="#" id="style8">
                        <img src="/assets/images/color-8.png" alt="">
                    </a>
                    <a href="#" id="style9">
                        <img src="/assets/images/color-9.png" alt="">
                    </a>
                    <a href="#" id="style10">
                        <img src="/assets/images/color-10.png" alt="">
                    </a>
                </div>
                <div class="box-title">
                    盒子型背景
                </div>
                <div class="images boxed-patterns">
                    <a href="#" id="bg_style_1">
                        <img src="/assets/images/bg.png" alt="">
                    </a>
                    <a href="#" id="bg_style_2">
                        <img src="/assets/images/bg_2.png" alt="">
                    </a>
                    <a href="#" id="bg_style_3">
                        <img src="/assets/images/bg_3.png" alt="">
                    </a>
                    <a href="#" id="bg_style_4">
                        <img src="/assets/images/bg_4.png" alt="">
                    </a>
                    <a href="#" id="bg_style_5">
                        <img src="/assets/images/bg_5.png" alt="">
                    </a>
                </div>
                <div class="style-options">
                    <a href="#" class="clear_style"> 清除样式</a>
                    <a href="#" class="save_style"> 保存样式</a>
                </div>
            </div>
            <div class="style-toggle open"></div>
        </div>
    </div>
</div>