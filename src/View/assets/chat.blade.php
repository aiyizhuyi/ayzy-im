<script src="/assets/plugins/layim/js/strophe-1.2.8.min.js"></script>
<script>
    var WebIM       = {};
    WebIM.config    = {};
    let WebIMConfig = {};
    let init        = function (){
        $.ajax({
            type: 'POST',
            url: '/im/chat/config',
            dataType: 'json',
            async: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
        }).done(function (m){
            WebIMConfig  = m;
            WebIM.config = WebIMConfig.WebIM;
            options      = {
                apiUrl: WebIMConfig.WebIM.apiURL,
                user: WebIMConfig.user,
                accessToken: WebIMConfig.token,
                appKey: WebIMConfig.WebIM.appkey
            };
        });
    }();
</script>
<script type='text/javascript' src='/assets/plugins/layim/js/websdk-1.4.13.js'></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/amr-player-master/amrnb.js"></script>
<script src="https://s3.cn-north-1.amazonaws.com.cn/ayzypublic/v1/plugins/amr-player-master/amrplayer.js"></script>
<script>
    let chat = function (){
        let myDate                  = new Date();
        let currentTimestamp        = Math.round(myDate.getTime() / 1000);
        let avatar                  = WebIMConfig.customerServiceAvatar;
        let expressionURI           = WebIMConfig.expressionURI;
        let messageInterval         = WebIMConfig.messageInterval;
        let limit                   = WebIMConfig.messageLimit;
        let assistURI               = WebIMConfig.assistURI;
        let offset                  = 0;
        let to                      = 'oGGyB1DHogWRfOY1b2VZtbP51lZA';
        let froms                   = WebIMConfig.user;
        let expressionAr            = WebIMConfig.expressionAr;
        let week                    = WebIMConfig.week;
        let removeUnReadMessageFlag = false;
        let conn                    = new WebIM.connection({
            isMultiLoginSessions: WebIM.config.isMultiLoginSessions,
            https: typeof WebIM.config.https === 'boolean' ? WebIM.config.https : location.protocol === 'https:',
            url: WebIM.config.xmppURL,
            heartBeatWait: WebIM.config.heartBeatWait,
            autoReconnectNumMax: WebIM.config.autoReconnectNumMax,
            autoReconnectInterval: WebIM.config.autoReconnectInterval,
            apiUrl: WebIM.config.apiURL,
            isAutoLogin: true
        });
        let leftindex               = null;
        let initflag                = false;
        // 环信聊天监听
        let huanxin                 = function (){
            conn.listen({
                //连接成功回调
                onOpened: function (message){
                    // 如果isAutoLogin设置为false，那么必须手动设置上线，否则无法收消息
                    // 手动上线指的是调用conn.setPresence(); 如果conn初始化时已将isAutoLogin设置为true
                    // 则无需调用conn.setPresence();
                },
                //连接关闭回调
                onClosed: function (message){
                    console.log(message);
                },
                //收到文本消息
                onTextMessage: function (message){
                    onTextMessage(message);
                },
                //收到表情消息
                onEmojiMessage: function (message){
                    let data = message.data;
                    for ( let i = 0, l = data.length; i < l; i++ ) {
                        console.log(data[i]);
                    }
                },
                //收到图片消息
                onPictureMessage: function (message){
                },
                //收到文件消息
                onFileMessage: function (message){
                },
                //本机网络连接成功
                onOnline: function (){
                    console.log('本机网络连接成功');
                },
                //本机网络掉线
                onOffline: function (){
                    console.log('本机网络掉线');
                },
                //失败回调
                onError: function (message){
                    console.log('1:失败了');
                    console.log(message);
                },
                //收到消息送达服务器回执
                onReceivedMessage: function (message){
                    console.log('收到消息送达服务器回执');
                },
                //收到消息送达客户端回执
                onDeliveredMessage: function (message){
                    console.log('收到消息送达客户端回执');
                },
                //收到消息已读回执
                onReadMessage: function (message){
                    console.log('收到消息已读回执');
                },
            });
            conn.open(options);
        };
        // 单聊发送文本消息
        let sendPrivateText         = function (text){
            let id  = conn.getUniqueId(); // 生成本地消息id
            let msg = new WebIM.message('txt', id);// 创建文本消息
            msg.set({
                msg: text,// 消息内容
                to: to,// 接收消息对象（用户id）
                roomType: false,
                success: function (id, serverMsgId){
                    saveMessage({
                        data: text,
                        error: false,
                        errorCode: '',
                        errorText: '',
                        ext: {
                            username: options.user,
                            avatar: avatar,
                            weichat: { originType: 'webim' }
                        },
                        from: options.user,
                        id: serverMsgId,
                        sourceMsg: text,
                        to: to,
                        type: 'chat'
                    });
                    text = faceReplace(text);
                    $('#user-list .list-group-item').each(function (i, v){
                        $(v).css('background', '#FFF');
                        if ( $(v).data('id').toString().toLowerCase() === to.toString().toLowerCase() ) {
                            $(v).find('.-user-list-time').html(zeroPadding(myDate.getHours()) + ':' + zeroPadding(myDate.getMinutes()));
                            $(v).find('.-user-list-chat').html(voiceReplace(imgReplace(text, true), true));
                            $(v).css('background', '#EEE');
                            $('#user-list').prepend(v);
                        }
                    });
                    text          = imgReplace(text);
                    let timestamp = $('.-time div:last').data('timestamp');
                    let msg       = judgmentTime(timestamp, Math.round(myDate.getTime() / 1000));
                    msg += messageView('self', text, avatar);
                    $('#-message').append(msg);
                    setEnd();
                },
                fail: function (e){
                    console.log("Send private text error");
                }
            });
            msg.body.chatType = 'singleChat';
            conn.send(msg.body);
        };
        // 补全前导0
        let zeroPadding             = function (number, option){
            number = parseInt(number);
            if ( option ) number += 1; // 是否将原先的值加一
            if ( number < 10 && number >= 0 ) {
                return '0' + number;
            }
            return number;
        };
        // 保存聊天记录
        let saveMessage             = function (message, fun = null){
            message.timestamp = Math.round(myDate.getTime() / 1000);
            $.ajax({
                type: 'POST',
                url: '/im/chat/save',
                data: { data: message },
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
            }).done(function (m){
                switch ( m.status ) {
                    case 0:
                        (fun === null) || fun();
                        break;
                }
            });
        };
        // 获取右侧内容
        let assistView              = function (){
            $.ajax({
                type: 'POST',
                url: assistURI,
                data: { username: to },
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
            }).done(function (m){
                $('#assists').html(m);
                // 选择常用语
                $('#myTab2_example2 .well').off().on('click', function (){
                    $('.-chat-input').text($(this).html());
                    $('.-chat-input').focus();
                });
                $('#myTab2_example3 .btn').off().on('click', function (){
                    let formdata = $(this).parents('form').serialize() + '&username=' + to;
                    $.ajax({
                        type: 'POST',
                        url: '/im/record',
                        data: formdata,
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        success: function (){
                            swal('保存成功！', '', 'success');
                        }
                    })
                    return false;
                });
                assistFuncEvent();
            });
        }
        // 收到聊天消息
        let onTextMessage           = function (message){
            let str  = faceReplace(message.sourceMsg);
            let bool = true;
            $('#user-list .list-group-item').each(function (i, v){
                if ( $(v).data('id').toString().toLowerCase() === message.from.toLowerCase() ) {
                    $(v).find('.-user-list-nickname').html(message.ext.username);
                    $(v).find('.-user-list-avatar img').attr('src', message.ext.avatar);
                    if ( to.toString().toLowerCase() !== message.from.toString().toLowerCase() ) {
                        let num = 0;
                        if ( $(v).find('.-user-list-avatar .-avatar-corner').length === 0 ) {
                            $(v).find('.-user-list-avatar').append('<div class="-avatar-corner">' + (parseInt(num) + 1) + '</div>');
                        } else {
                            num = $(v).find('.-user-list-avatar .-avatar-corner').html();
                            if ( parseInt(num) > 97 ) num = 98;
                            $(v).find('.-user-list-avatar .-avatar-corner').html(parseInt(num) + 1);
                        }
                        saveUnreadMessageNum({ fromId: message.from, kefuId: message.to, num: ++num });
                    }
                    $(v).find('.-user-list-time').html(zeroPadding(myDate.getHours()) + ':' + zeroPadding(myDate.getMinutes()));
                    $(v).find('.-user-list-chat').html(voiceReplace(imgReplace(str, true), true));
                    $('#user-list').prepend(v);
                    bool = false;
                }
            });
            if ( bool ) {
                let html = messageView('group-item', voiceReplace(imgReplace(str, true), true), message.ext.avatar, message.from, message.ext.username);
                $('#user-list').prepend(html);
                event(false);
            }
            str = imgReplace(str);
            str = voiceReplace(str);
            str = lineFeedReplace(str);
            if ( to.toString().toLowerCase() === message.from.toString().toLowerCase() ) {
                let timestamp = $('.-time div:last').data('timestamp');
                let msg       = judgmentTime(timestamp, Math.round(myDate.getTime() / 1000))
                msg += messageView('other', str, $('#uio-' + message.from).find('img').attr('src'));
                $('#-message').append(msg);
                setEnd();
            }
        };
        // 所有事件
        let event                   = function (bool = true, init = true){
            // 监听键盘回车事件，进行发送
            $('.-chat-input').off().on('keydown', function (event){
                if ( event.keyCode == 13 ) {
                    let s = $(this).html()
                    if ( s === null || s === '' ) return;
                    let dd  = s.replace(/<\/?.+?>/g, "");
                    let dds = dd.replace(/ /g, "");//dds为得到后的内容
                    if ( dds !== '' ) sendPrivateText(dds);
                    $(this).html('');
                }
            });
            // 调出表情面板
            $('.-chat-sub-input .fa-meh-o').off().on('click', function (){
                if ( $('.-chat-sub-input').find('.display').css('display') === 'none' ) {
                    $('.-chat-sub-input').find('.display').show();
                } else {
                    $('.-chat-sub-input').find('.display').hide();
                }
            });
            // 上传图片
            $('.-chat-sub-input .fa-image').off().on('click', function (){
                $('#imgFile').click();
            });
            $('#imgFile').off().on('change', function (){
                let fileObj = document.getElementById("imgFile").files[0]; // js 获取文件对象
                if ( typeof (fileObj) === "undefined" || fileObj.size <= 0 ) {
                    alert("请选择图片");
                    return;
                }
                let formFile = new FormData();
                formFile.append("action", "UploadVMKImagePath");
                formFile.append("file", fileObj); //加入文件对象
                $.ajax({
                    type: 'POST',
                    url: '/im/chat/upload',
                    data: formFile,
                    dataType: 'json',
                    cache: false,
                    processData: false,
                    contentType: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                }).done(function (m){
                    switch ( m.status ) {
                        case 0:
                            let data = m.data;
                            let text = '';
                            for ( let i in data )
                                sendPrivateText(data[i]);
                            break;
                    }
                });
            });
            // 选择表情
            $('.-expression div').off().on('click', function (){
                let alt = $(this).find('img').attr('alt');
                $('.-chat-input').append('face' + alt).focus();
                $('.-chat-sub-input').find('.display').hide();
            });
            // 选择某一用户进行查看聊天记录且回复
            $('#user-list .list-group-item').off().on('click', function (){
                let _this = this;
                to        = $(this).data('id');
                let html  = $(this).find('.-user-list-nickname').html();
                $('.-chat-info').find('.-chat-info-heading').html(html.substring(0, html.indexOf('<br>')));
                if ( leftindex != $(_this).index() ) {
                    leftindex = $(_this).index();
                    assistView();
                }
                offset = 0;
                $.ajax({
                    type: 'POST',
                    url: '/im/chat/getMessage',
                    data: { to: to, from: froms },
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                }).done(function (m){
                    switch ( m.status ) {
                        case 0:
                            offset += parseInt(limit);
                            $(_this).find('.-user-list-avatar .-avatar-corner').remove();
                            $('#user-list .list-group-item').css('background', '#FFF');
                            let data = m.data;
                            let html = messageView('more');
                            $(_this).css('background', '#EEE');
                            let timestamp = 'undefined';
                            for ( let i in data ) {
                                let text        = faceReplace(data[i].sourceMsg);
                                let messageTime = new Date(data[i].timestamp * 1000);
                                $(_this).find('.-user-list-time').html(judgmentTime2(currentTimestamp, Math.round(messageTime.getTime() / 1000)));
                                $(_this).find('.-user-list-chat').html(voiceReplace(imgReplace(text, true), true));
                                text = imgReplace(text);
                                text = voiceReplace(text);
                                text = lineFeedReplace(text);
                                // 自己的数据
                                if ( data[i].from.toString().toLowerCase() === froms.toString().toLowerCase() ) {
                                    html += judgmentTime(timestamp, data[i].timestamp);
                                    html += messageView('self', text, avatar);
                                }
                                // 别人的数据
                                else if ( data[i].to.toString().toLowerCase() === froms.toString().toLowerCase() ) {
                                    html += judgmentTime(timestamp, data[i].timestamp);
                                    let patientAvatar;
                                    if ( typeof data[i].ext === 'undefined' )
                                        patientAvatar = $(_this).find('.-user-list-avatar img').attr('src');
                                    else {
                                        patientAvatar = data[i].ext.avatar;
                                    }
                                    html += messageView('other', text, patientAvatar);
                                }
                                timestamp = data[i].timestamp;
                            }
                            $('#-message').html(html);
                            setEnd();
                            moreMessageEvent();
                            break;
                    }
                });
                if ( removeUnReadMessageFlag ) {
                    removeUnreadMessage({ 'patient': $(this).data('id') });
                } else {
                    removeUnReadMessageFlag = true;
                }
            });
            if ( init ) {
                if ( bool ) {
                    $('#user-list .list-group-item').trigger("click");
                } else {
                    $('#user-list .list-group-item:last').trigger("click");
                }
            }
        };
        let moreMessageEvent        = function (){
            $('.-chat-info-message-more').off().on('click', function (){
                getMessage(this);
            });
        };
        let assistFuncEvent         = function (){
            $('.-assist-info .-assist-info-heading').off().on('click', function (){
                if ( $(this).next().css('display') === 'none' ) {
                    $('.-assist-info .-assist-info-body').hide();
                    $(this).next().show();
                } else {
                    $('.-assist-info .-assist-info-body').hide();
                }
            });
            $('.nav-tabs li').off().on('click', function (){
                if ( !$(this).hasClass('active') ) {
                    $('.nav-tabs li').removeClass('active');
                    $('.tab-content').find('.tab-pane').removeClass('active in');
                    let id = $(this).find('a').attr('href');
                    $(this).addClass('active');
                    $(id).addClass('active in');
                }
            });
        }
        let setEnd                  = function (height){
            let div       = document.getElementById('-message');
            div.scrollTop = height || div.scrollHeight;
        };
        let getMessage              = function (_this){
            $.ajax({
                type: 'POST',
                url: '/im/chat/getMessage',
                data: { to: to, from: froms, limit: limit, offset: offset },
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
            }).done(function (m){
                switch ( m.status ) {
                    case 0:
                        let data      = m.data;
                        offset += parseInt(limit);
                        let html      = '';
                        let timestamp = 'undefined';
                        for ( let i in data ) {
                            let text = faceReplace(data[i].sourceMsg);
                            text     = imgReplace(text);
                            text     = voiceReplace(text);
                            text     = lineFeedReplace(text);
                            // 自己的数据
                            if ( data[i].from.toString().toLowerCase() === froms.toString().toLowerCase() ) {
                                html += judgmentTime(timestamp, data[i].timestamp);
                                html += messageView('self', text, avatar);
                            }
                            // 别人的数据
                            else if ( data[i].to.toString().toLowerCase() === froms.toString().toLowerCase() ) {
                                html += judgmentTime(timestamp, data[i].timestamp);
                                html += messageView('other', text, data[i].ext.avatar);
                            }
                            timestamp = data[i].timestamp;
                        }
                        let height1 = 0;
                        $('#-message li').each(function (i, v){
                            height1 += $(v).height();
                        });
                        $('#-message').prepend(html).prepend(_this);
                        let height2 = 0;
                        $('#-message li').each(function (i, v){
                            height2 += $(v).height();
                        });
                        if ( data.length === 0 || data.length < parseInt(limit) ) {
                            $(_this).remove();
                        }
                        let interval = height2 - height1;
                        if ( interval !== 0 ) setEnd(interval);
                        break;
                }
            });
        }
        // 视图
        let messageView             = function (option, text, avatars, froms, username, time = (zeroPadding(myDate.getHours()) + ':' + zeroPadding(myDate.getMinutes())), corner = false){
            switch ( option ) {
                case 'self':
                    return "<li class=\"-self\">" +
                        "    <div class=\"-messages\">" +
                        "        <p>" + text + "</p>" +
                        "    </div>" +
                        "    <div class=\"-avatar\">" +
                        "        <img src=\"" + avatars + "\" alt=\"\">" +
                        "    </div>" +
                        "</li>";
                case 'other':
                    return "<li class=\"-other\">" +
                        "    <div class=\"-avatar\">" +
                        "        <img src=\"" + avatars + "\" alt=\"\">" +
                        "    </div>" +
                        "    <div class=\"-messages\">" +
                        "        <p>" + text + "</p>" +
                        "    </div>" +
                        "</li>";
                case 'group-item':
                    if ( corner === false ) corner = ((text === '') ? '' : '<div class="-avatar-corner">1</div>')
                    else corner = '';
                    return '<li class="list-group-item" data-id="' + froms + '" id="uio-' + froms + '">' +
                        '     <div class="-user-list-avatar">' +
                        '         <img src="' + avatars + '" alt="">' + corner +
                        '     </div>' +
                        '     <div class="-user-list-username">' +
                        '         <div class="-user-list-nickname">' + username + '</div>' +
                        '         <div class="-user-list-time">' + time + '</div>' +
                        '         <div class="-user-list-chat">' + text + '</div>' +
                        '         <div class="-user-list-close"></div>' +
                        '     </div>' +
                        '</li>';
                case 'more':
                    return '<li class="-chat-info-message-more">获取更多</li>';
                default:
                    return '';
            }
        }
        // 加载左侧用户名称
        let loadName                = function (){
            $.ajax({
                type: 'POST',
                url: '/im/chat/getUser',
                data: {},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
            }).done(function (m){
                switch ( m.status ) {
                    case 0:
                        let data = m.data;
                        let html = '';
                        for ( let i in data ) {
                            let nickname = data[i].nickname;
                            nickname += '<br><span style="color:rgba(174,174,174,0.85);">' + data[i].tel + '</span>';
                            let message  = data[i].message;
                            message      = faceReplace(message);
                            message      = imgReplace(message, true);
                            message      = voiceReplace(message, true);
                            html += messageView('group-item', message, data[i].avatar, data[i].username, nickname, judgmentTime2('sdflaksjdf', data[i].timestamp), true);
                        }
                        $('#user-list').html(html);
                        event(false);
                        getUnreadMessageNum();
                        break;
                }
            });
        };
//      删除未读消息
        let removeUnreadMessage     = function (data){
            $.ajax({
                type: 'post',
                url: '/unread/messages/remove',
                data: data,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
            })
        }
//      存储未读信息
        let saveUnreadMessageNum    = function (data){
            $.ajax({
                type: 'post',
                url: '/unread/messages',
                data: data,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
            })
        }
//      获取没读信息
        let getUnreadMessageNum     = function (){
            $.ajax({
                type: 'get',
                url: '/unread/messages',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
            }).done(function (m){
                $('#user-list li').each(function (){
                    for ( let o in m ) {
                        if ( $(this).data('id') == o ) {
                            $(this).find('.-user-list-avatar').append('<div class="-avatar-corner">' + m[o] + '</div>')
                        }
                    }
                })
            });
        }
        // 表情库
        let faces                   = function (){
            let alt  = expressionAr,
                arr  = [];
            let html = '';
            for ( let i  in alt ) {
                html += '<div><img src="' + expressionURI + i + '.gif' + '" alt="' + alt[i] + '"></div>';
                arr[alt[i]] = expressionURI + i + '.gif';
            }
            $('.-expression').html(html);
            return arr;
        };
        // 解析表情
        let faceReplace             = function (str){
            if ( str === null || str === undefined ) return '';
            let alt = expressionAr;
            let m   = str.match(/face\[.+?\]/g);
            if ( m === null ) return str;
            for ( let i in m ) {
                let j = m[i].substr(4);
                str   = str.replace(m[i], '<img src="' + expressionURI + alt.indexOf(j) + '.gif" />')
            }
            return str;
        };
        // 解析换行符
        let lineFeedReplace         = function (str){
            return str.replace(/[\n\r]/g, '<br>');
        }
        // 解析图片
        let imgReplace              = function (str, bool = false){
            if ( str === null || str === undefined ) return '';
            let m = str.match(/img\[.+?\]/g)
            if ( m === null ) return str;
            for ( let i in m ) {
                let j = m[i];
                j     = j.substring(4, j.length - 1);
                str   = str.replace(m[i], (bool ? '[图片]' : '<img src="' + j + '" class="img" />'));
            }
            return str;
        };
        // 解析声音
        let voiceReplace            = function (str, bool = false){
            if ( str === null || str === undefined ) return '';
            let m = str.match(/voice\[.+?\]/g);
            if ( m === null ) return str;
            for ( let i in m ) {
                let j = m[i];
                j     = j.substring(6, j.length - 1);
                str   = str.replace(m[i], (bool ? '[音频]' : '<div class="-volume" data-url=' + j + '> <i class="fa fa-volume-up fa-1x"></i> </div>'));
            }
            return str;
        };
        // 播放声音
        let voicePlay               = function (){
            $(document).off().on('click', '.-volume', function (){
                let player = new AmrPlayer($(this).data('url'));
                setTimeout(function (){
                    player.play();
                }, 1000);
            });
        }
        // css 样式
        let cssStyle                = function (){
            let height = window.innerHeight || document.documentElement.clientHeight;
            $('.-info').css('height', (height
                - $(".topbar").height()
                - $(".topbar").height()
                - 80
                - $('._breadcrumb').height()
                - $('._breadcrumb').height()
                - $('.inner').height()
            ) + 'px');
        };
        // 显示消息时间
        let judgmentTime            = function (prevTime, msgTime){
            msgTime                 = parseInt(msgTime);
            let time                = '';
            // 当前PHP时间戳
            let currentPHPTimestamp = Math.round(myDate.getTime() / 1000);
            // 两条消息的时间间隔 分钟为单位
            prevTime                = parseInt(prevTime);
            // 消息时间戳
            let messageTime         = new Date(msgTime * 1000);
            // 今日凌晨的时间
            let currentDayZero      = new Date(myDate.toLocaleDateString());
            switch ( true ) {
                // 当天
                case (messageTime.getTime() > currentDayZero.getTime()):
                    time = zeroPadding(messageTime.getHours()) + ':' + zeroPadding(messageTime.getMinutes());
                    break;
                // 昨天
                case (messageTime.getTime() > (currentDayZero.getTime() - 86400000)):
                    time = '昨天 ' + zeroPadding(messageTime.getHours()) + ':' + zeroPadding(messageTime.getMinutes());
                    break;
                // 本周
                case (messageTime.getTime() > (currentDayZero.getTime() - (7 * 86400000))):
                    time = week[messageTime.getDay()] + ' ' + zeroPadding(messageTime.getHours()) + ':' + zeroPadding(messageTime.getMinutes());
                    break;
                // 显示全部
                default:
                    time = messageTime.getFullYear() +
                        '年' + zeroPadding(messageTime.getMonth(), true) +
                        '月' + zeroPadding(messageTime.getDate()) +
                        '日 ' + zeroPadding(messageTime.getHours()) +
                        ':' + zeroPadding(messageTime.getMinutes());
            }
            // 没有上一个时间戳
            if ( isNaN(prevTime) ) {
                return '<li class="-time">' + '<div data-timestamp="' + msgTime + '">' + time + '</div>' + '</li>';
            }
            // 时间间隔大于时间间隔显示时间
            else if ( Math.round((msgTime - prevTime) / 60) >= messageInterval ) {
                return '<li class="-time">' + '<div data-timestamp="' + msgTime + '">' + time + '</div>' + '</li>';
            }
            // 返回空
            else {
                return '';
            }
        };
        // 显示左侧消息时间
        let judgmentTime2           = function (prevTime, msgTime){
            msgTime                 = parseInt(msgTime);
            let time                = '';
            // 当前PHP时间戳
            let currentPHPTimestamp = Math.round(myDate.getTime() / 1000);
            // 两条消息的时间间隔 分钟为单位
            prevTime                = parseInt(prevTime);
            // 消息时间戳
            let messageTime         = new Date(msgTime * 1000);
            // 今日凌晨的时间
            let currentDayZero      = new Date(myDate.toLocaleDateString());
            switch ( true ) {
                // 当天
                case (messageTime.getTime() > currentDayZero.getTime()):
                    time = zeroPadding(messageTime.getHours()) + ':' + zeroPadding(messageTime.getMinutes());
                    break;
                // 昨天
                case (messageTime.getTime() > (currentDayZero.getTime() - 86400000)):
                    time = '昨天 ' + zeroPadding(messageTime.getHours()) + ':' + zeroPadding(messageTime.getMinutes());
                    break;
                // 本周
                case (messageTime.getTime() > (currentDayZero.getTime() - (7 * 86400000))):
                    time = week[messageTime.getDay()] + ' ' + zeroPadding(messageTime.getHours()) + ':' + zeroPadding(messageTime.getMinutes());
                    break;
                // 本月
                case (messageTime.getTime() > (currentDayZero.getTime() - (30 * 86400000))):
                    time = zeroPadding(messageTime.getMonth(), true) + '月' + zeroPadding(messageTime.getDate()) + '日 ';
                    //time = zeroPadding(messageTime.getDate()) + '日 ' + ' ' + zeroPadding(messageTime.getHours()) + ':' + zeroPadding(messageTime.getMinutes());
                    break;
                // 本年
                case (messageTime.getTime() > (currentDayZero.getTime() - (365 * 86400000))):
                    time = zeroPadding(messageTime.getMonth(), true) + '月' + zeroPadding(messageTime.getDate()) + '日 '; //  + ' ' + zeroPadding(messageTime.getHours()) + ':' + zeroPadding(messageTime.getMinutes());
                    break;
                // 显示全部
                default:
                    time = messageTime.getFullYear() +
                        '年' + zeroPadding(messageTime.getMonth(), true) +
                        '月' + zeroPadding(messageTime.getDate()) +
                        '日 ';//  + zeroPadding(messageTime.getHours()) +
                        //':' + zeroPadding(messageTime.getMinutes());
            }
            return time;
        };
        let _setInterval            = function (){
            setInterval(function (){
                myDate           = new Date();
                currentTimestamp = Math.round(myDate.getTime() / 1000);
            }, 1000);
        };
        return {
            init: function (){
                huanxin();
                faces();
                loadName();
                event();
                cssStyle();
                _setInterval();
                voicePlay();
            }
        }
    }();
    $(function (){
        Main.init();
        chat.init();
    });
</script>
