<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>聊天</title>
    <link rel="stylesheet" href="{{ asset('/assets/plugins/layim/layui/css/layui.mobile.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/layim/css/menu.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/layim/css/contextMenu.css') }}">
</head>
<body>
<script type='text/javascript' src='{{ asset('/assets/plugins/layim/js/webim.config.js') }}'></script>
<script type='text/javascript' src='{{ asset('/assets/plugins/layim/js/strophe-1.2.8.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('/assets/plugins/layim/js/websdk-1.4.13.js') }}'></script>
<script src="{{ asset('/assets/plugins/layim/layui/layui.js') }}"></script>
<script>

    //layui绑定扩展
    layui.config({
        base: '{{ asset('/assets/plugins/layim/js') }}/'
    }).extend({
        socket: 'socket',
    });
    layui.use(['mobile', 'socket', 'jquery'], function (mobile, socket) {
        var mobile = layui.mobile, layim = mobile.layim, $ = layui.jquery;
        var socket = layui.socket;

        socket.config({
            user: '{{ $info['openId'] }}',
            pwd: 'uio234',
            layim: layim,
        });
        //基础配置

        layim.config({
            init: {
                mine: {
                    'id': '{{ $info['openId'] }}',
                    "username": "{{ $info['nickname'] }}" //我的昵称
                    , "avatar": "{{ $info['headimgurl'] }}" //我的头像
                },
                url: 'class/doAction.php?action=get_user_data', data: {}
            }
            //上传图片接口
            , uploadImage: {
                url: '{{ route('chatUpload') }}' //（返回的数据格式见下文）
                , type: 'post' //默认post
            }
            //上传文件接口
            , uploadFile: {
                url: '{{ route('chatUpload') }}' //
                , type: 'post' //默认post
            }
            //提交建群信息
            , commitGroupInfo: {
                url: 'class/doAction.php?action=commitGroupInfo'
                , type: 'get' //默认
            }
            //获取系统消息
            , getMsgBox: {
                url: 'class/doAction.php?action=getMsgBox'
                , type: 'get' //默认post
            }
            //获取总的记录数
            , getChatLogTotal: {
                url: 'class/doAction.php?action=getChatLogTotal'
                , type: 'get' //默认post
            }
            //获取历史记录
            , getChatLog: {
                url: 'class/doAction.php?action=getChatLog'
                , type: 'get' //默认post
            }
            , isAudio: true //开启聊天工具栏音频
            , isVideo: false //开启聊天工具栏视频
            , groupMembers: true
            //扩展工具栏
            // , tool: [{
            //         alias: 'code'
            //         , title: '代码'
            //         , icon: '&#xe64e;'
            //     }]
            , title: 'layim'
            , copyright: true
            , initSkin: '1.jpg' //1-5 设置初始背景
            , notice: true //是否开启桌面消息提醒，默认false
            , systemNotice: false //是否开启系统消息提醒，默认false
            , msgbox: layui.cache.dir + 'css/modules/layim/html/msgbox.html' //消息盒子页面地址，若不开启，剔除该项即可
            , find: layui.cache.dir + 'css/modules/layim/html/find.html' //发现页面地址，若不开启，剔除该项即可
            , chatLog: layui.cache.dir + 'css/modules/layim/html/chatlog.html' //聊天记录页面地址，若不开启，剔除该项即可
            , createGroup: layui.cache.dir + 'css/modules/layim/html/createGroup.html' //创建群页面地址，若不开启，剔除该项即可
            , Information: layui.cache.dir + 'css/modules/layim/html/getInformation.html' //好友群资料页面
        });
        //创建一个会话

        layim.chat({
            id: @if(array_key_exists('code',$kefu)) '000' @else '{{ $kefu['id'] }}'  @endif
            , name: '客服人员'
            , type: 'kefu' //friend、group等字符，如果是group，则创建的是群聊
            , avatar: 'http://wx.qlogo.cn/mmopen/FCZUncnI4CQUbtGHZS2iagPQ0y28xNUpa8fyiac7NVq6eoUWvdYiaFkcVnDJAQGWMvdbGXEt6K9NaOSJl9f5yib7TZVxu4jsDY2E/132'
        });
        @if(!is_array($kefu))
                $(function () {
                    $.ajax({
                        type: 'POST',
                        url: '/im/chat/kefu',
                        data: {kefu: '{{ $kefu['id'] }}', user: '{{ $info['openId'] }}', num: {{ $kefu['number'] }}},
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                });
        @endif

    });

</script>
</body>
</html>


