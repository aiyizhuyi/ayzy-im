@extends('base')

@section('head')
    <style>
        .panel {
            border-radius: 0px;
        }

        .-info {
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: flex-start;
            align-items: flex-start;
            flex-flow: row wrap;
        }

        .-chat-person {
            display: flex;
            width: 30%;
            overflow: auto;
            height: 100%;
            border-top: 1px solid #ccc;
            border-left: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        .-chat-person .list-group {
            width: 100%;
        }

        .-assist-info {
            width: 100%;
            border-top: 1px solid #ccc;
            border-right: 1px solid #ccc;
            overflow: auto;
        }

        .-assist-info .-assist-info-heading {
            display: flex;
            width: 100%;
            height: 3em;
            line-height: 3em;
            font-size: 14px;
            justify-content: flex-start;
            align-items: center;
            border-bottom: 1px solid #ededed;
            padding-left: 15px;
            cursor: pointer;
        }

        .-assist-info::first-child {
            border-top-right-radius: 5px;
        }

        .-assist-info::last-child {
            border-bottom-right-radius: 5px;
        }

        .-assist-info .-assist-info-body {
            display: none;
            overflow: auto;
            width: 100%;
        }

        .-chat-info {
            width: 70%;
            height: 100%;
            overflow: auto;
            border: 1px solid #ccc;
            position: relative;
        }

        .-chat-input {
            width: 100%;
            height: 6em;
            overflow: auto;
            padding: 0.1em 0.2em 0.1em 15px;
            line-height: 1.5em;
            display: flex;
            justify-content: flex-start;
            align-items: flex-start;
            outline: none;
        }

        .-chat-sub-input {
            height: 2.5em;
            width: 100%;
            display: flex;
            justify-content: flex-start;
            align-items: center;
            flex-flow: row nowrap;
            border-top: 1px solid #ededed;
            padding-left: 15px;
            position: relative;
        }

        .-chat-sub-input i {
            cursor: pointer;
            font-size: 19px;
        }

        .-chat-sub-input .display {
            display: none;
        }

        .-expression {
            position: absolute;
            width: 300px;
            height: 205px;
            background: #fefefe;
            left: 15px;
            bottom: calc(2.5em + 15px);
            border-radius: 5px;
            box-shadow: 1px 1px 8px -2px #000;
            display: flex;
            justify-content: flex-start;
            align-items: flex-start;
            flex-flow: row wrap;
            padding: 5px;
        }

        .-info > .panel::first-child {
            width: 70%;
        }

        .-info > .panel::last-child {
            width: 30%;
        }

        .-expression div {
            margin: 1px;
            cursor: pointer;
        }

        .-expression:after {
            border-style: solid;
            border-width: 5px;
            content: "";
            height: 0;
            position: absolute;
            bottom: -10px;
            width: 0;
            border-color: #fefefe transparent transparent #fefefe;
            left: 5px;
        }

        .-chat-sub-input i {
            margin-right: 10px;
        }

        .-chat {
            display: flex;
        }

        .list-group .list-group-item:first-child {
            border-top-right-radius: 0;
            border-top: none;
        }

        .list-group .list-group-item:last-child {
            border-bottom-right-radius: 0;
        }

        .list-group .list-group-item {
            border-right: none;
            border-left: none;
        }

        .list-group {
            height: 100%;
            overflow: auto;
        }

        .-chat-info-heading {
            width: 100%;
            height: 4em;
            display: flex;
            align-items: center;
            justify-content: flex-start;
            flex-flow: row nowrap;
            font-size: 15px;
            padding: 15px;
            border-bottom: 1px solid #ededed;
        }

        .-chat-show {
            width: 100%;
            overflow: auto;
            height: calc(100% - 12.5em - 30px);
            list-style: none;
            padding: 0px;
        }

        .-chat-show li {
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: flex-start;
            padding: 10px;
        }

        .-chat-show .-other {
            overflow: hidden;
        }

        .-chat-show .-avatar {
            width: 40px;
            height: 40px;
            border-radius: 50%;
        }

        .-chat-show .-avatar img {
            width: 100%;
            height: 100%;
            border-radius: 50%;
        }

        .-chat-show .-messages {
            max-width: calc(100% - 125px);
            display: flex;
            padding: 5px;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
            position: relative;
        }

        .-chat-show .-messages p {
            margin: 0;
            padding: 5px;
        }

        .-chat-show .-messages .img {
            width: 150px;
            cursor: pointer;
        }

        .-chat-show .-other .-messages {
            border-top-right-radius: 5px;
            border-top-left-radius: 5px;
            background: #e5e5ea;
            margin-left: 15px;
        }

        .-chat-show .-self .-messages {
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            background: #5F8295;
            margin-right: 15px;
            color: #fff;
        }

        .-chat-show .-messages:after {
            border-style: solid;
            border-width: 5px;
            content: "";
            height: 0;
            position: absolute;
            top: 10px;
            width: 0;
        }

        .-chat-show .-other .-messages:after {
            border-color: #E5E5EA #E5E5EA transparent transparent;
            left: -10px;
        }

        .-chat-show .-self .-messages:after {
            border-color: #5F8295 transparent transparent #5F8295;
            right: -10px;
        }

        .-chat-show .-self {
            displa: flex;
            align-items: flex-start;
            justify-content: flex-end;
        }

        .list-group {
            margin-bottom: 0px;
        }

        .list-group .list-group-item {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .-user-list-avatar {
            width: 50px;
            height: 50px;
            position: relative;
        }

        .-user-list-avatar img {
            width: 100%;
            height: 100%;
            border-radius: 5px;
            cursor: pointer;
        }

        .-user-list-avatar .-avatar-corner {
            position: absolute;
            background: red;
            font-size: 10px;
            width: 1.5em;
            top: -0.5em;
            right: -0.5em;
            height: 1.5em;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            color: #fff;
        }

        .-user-list-username {
            display: flex;
            justify-content: flex-start;
            align-items: center;
            flex-flow: row wrap;
            height: 60px;
            width: calc(100% - 65px);
            position: relative;
            cursor: pointer;
        }

        .-chat-info-message-more {
            width: 100%;
            justify-content: center !important;
            align-items: center !important;
            font-size: 40%;
            padding: 0 !important;
            font-style: oblique;
            /*border-bottom: 1px solid #ededed;*/
            cursor: pointer;
            margin-bottom: 10px;
            margin-top: 10px;
            color: #bbbbbb;
        }

        .-user-list-nickname {
            width: 50%;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            /*-webkit-line-clamp: 1;*/
            /*overflow: hidden;*/
        }

        .-user-list-time {
            width: 50%;
            display: flex;
            align-items: center;
            justify-content: flex-end;
        }

        .-user-list-chat {
            width: 100%;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 1;
            overflow: hidden;
            font-size: 60%;
        }

        .-user-list-chat img {
            width: 1.5em;
        }

        .-user-list-close {
            position: absolute;
            right: -10px;
            top: 15px;
            width: 20px;
            height: 20px;
            background: rgba(0, 0, 0, .7);
            border-radius: 10px;
            color: #fff;
            cursor: pointer;
            transform: rotate(45deg);
            -ms-transform: rotate(45deg); /* IE 9 */
            -moz-transform: rotate(45deg); /* Firefox */
            -webkit-transform: rotate(45deg); /* Safari 和 Chrome */
            -o-transform: rotate(45deg); /* Opera */
            display: none;
        }

        .-user-list-close::after {
            content: '';
            width: 2px;
            height: 10px;
            background: #fff;
            left: 9px;
            top: 5px;
            position: absolute;
            border-radius: 2px;
        }

        .-user-list-close::before {
            content: '';
            width: 10px;
            height: 2px;
            background: #fff;
            position: absolute;
            top: 9px;
            left: 5px;
            border-radius: 2px;
        }

        .list-group-item:hover .-user-list-close {
            /*display: block;*/
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .height-100 {
            height: 100% !important;
            overflow: auto;
        }

        .width-30 {
            width: 30% !important;
        }

        .width-70 {
            width: 70% !important;
        }

        .no-margin {
            margin: none;
        }

        .-time {
            justify-content: center !important;
            align-items: center !important;
            padding: 0px !important;
        }

        .-time > div {
            display: flex;
            justify-content: center;
            align-items: center;
            background: rgba(0, 0, 0, .3);
            color: #fff;
            padding: 0px 15px;
            border-radius: 15px;
            font-size: 10px;
        }

        .well {
            margin-bottom: 10px;
        }

    </style>
@endsection
@section('container')
    <div class="toolbar row">
        <div class="col-sm-6 hidden-xs">
            <div class="page-header">
                <h1>咨询
                    <small>客服处理咨询</small>
                </h1>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <a href="#" class="back-subviews">
                <i class="fa fa-chevron-left"></i>
                返回
            </a>
            <a href="#" class="close-subviews">
                <i class="fa fa-times"></i>
                关闭
            </a>
            {{--<div class="toolbar-tools pull-right">--}}
            {{--<ul class="nav navbar-right">--}}
            {{--<li class="">--}}
            {{--<a href="javascript:;" class="add-user">--}}
            {{--<i class="fa fa-plus"></i>--}}
            {{--添加--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="row _breadcrumb">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li>
                    <a href="/">
                        首页
                    </a>
                </li>
                <li class="active">
                    咨询
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-white no-margin">
                <div class="panel-body content">
                    <div class="-info">
                        <div class="panel height-100 width-70 no-margin">
                            <div class="panel-body -chat no-padding height-100">
                                <div class="-chat-person">
                                    <ul class="list-group" id="user-list">
                                        <!-- 用户列表 -->
                                    </ul>
                                    {{--<div class="tabbable">--}}
                                    {{--<ul id="myTab" class="nav nav-tabs">--}}
                                    {{--<li class="active">--}}
                                    {{--<a href="#myTab_example1" data-toggle="tab">--}}
                                    {{--<i class="green fa fa-home"></i> 回复--}}
                                    {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                    {{--<a href="#myTab_example2" data-toggle="tab">--}}
                                    {{--未回复 <span class="badge badge-danger">4</span>--}}
                                    {{--</a>--}}
                                    {{--</li>--}}
                                    {{--</ul>--}}
                                    {{--<div class="tab-content">--}}
                                    {{--<div class="tab-pane fade in active" id="myTab_example1">--}}
                                    {{----}}
                                    {{--</div>--}}
                                    {{--<div class="tab-pane fade" id="myTab_example2">--}}

                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                </div>
                                <div class="-chat-info">
                                    <div class="-chat-info-heading"><!-- 咨询人名称  --></div>
                                    <ol class="-chat-show" id="-message">
                                        <!-- 聊天消息 -->
                                    </ol>
                                    <div class="-chat-sub-input">
                                        <!-- 操作图标  -->
                                        <i class="fa fa-meh-o"></i>
                                        <i class="fa fa-image"></i>
                                        <!-- 上传文件 -->
                                        {{--<i class="fa fa-file"></i>--}}
                                        <div class="display">
                                            <div class="-expression"><!-- 表情列表 默认隐藏 --></div>
                                        </div>
                                        <input type="file" id="imgFile" name="imgFile" accept="image/jpeg, image/png" style="z-index:-1">
                                    </div>
                                    <div class="-chat-input" contenteditable="true"><!-- 输入内容 --></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel width-30 height-100 no-margin">
                            <div class="panel-body no-padding height-100" id="assists">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('subview')
@endsection
@section('js-container')
    @include('Ayzy::assets.chat')
@endsection

