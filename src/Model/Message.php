<?php

namespace Ayzy\IM\Model;

class Message extends Model
{
    protected $table      = 'message';
    protected $collection = 'message';
    protected $fillable   = [];

    static public function add( $data )
    {
        $self = new self();
        foreach ( $data as $k => $v ) {
            $self->$k = $v;
        }
        $self->save();

        return $self;
    }
}