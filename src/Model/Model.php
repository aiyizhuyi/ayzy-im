<?php

namespace Ayzy\IM\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

abstract class Model extends Eloquent
{
    protected $connection = 'ayzymongodb';

    /**
     * 添加数据
     *
     * @param $data
     * @return bool
     */
    abstract public static function add( $data );

    /**
     * 编辑数据
     *
     * @param $id
     * @param $data
     * @return bool
     */
    public static function edit( $id, $data )
    {
        $self = $id;
        is_object($self) || $self = self::find($self);
        foreach ( $data as $k => $v ) $self->$k = $v;
        $result = $self->save();

        return $result;
    }

    /**
     * 全搜索条件
     *
     * @param array $where   ['is_active'=>1]
     * @param array $orderBy ['_id','desc']
     * @param int   $offset  0
     * @param int   $limit   0
     * @param array $like    ['search'=>'','searchField'=>[]]
     * @param array $with    ['GoodsName'] hasOne or hasMany with Model function. array['GoodsName','MemberName']
     * @return mixed query;
     */
    public static function whereLike( Array $where = [], Array $orderBy = ['_id', 'asc'], $offset = 0, $limit = 0, Array $like = [], Array $with = [] )
    {
        $ar = ['<>', '>', '<', '<=', '>=', '!=', '='];
        list($field, $sort) = $orderBy;
        $query = self::orderBy($field, $sort);
        foreach ( $with as $v ) $query = $query->with($v);
        foreach ( $where as $k => $v ) {
            if ( is_array($v) ) {
                empty(array_intersect($ar, $v)) ? $query = $query->whereIn($k, $v) : $query = $query->where($v[0], $v[1], $v[2]);
            } else {
                $query = $query->where($k, $v);
            }
        }
        $searchField = [];
        $search      = '';
        extract($like);
        if ( !empty($search) ) {
            $GLOBALS['search'] = '%' . $search . '%';
            $GLOBALS['field']  = $searchField;
            $query             = $query->where(function( $query ) {
                foreach ( $GLOBALS['field'] as $key => $val ) {
                    ( 0 == $key ) ? $query->where($val, 'like', $GLOBALS['search']) : $query->orwhere($val, 'like', $GLOBALS['search']);
                }
            });
        }
        $total = $query->count();
        if ( $total == $offset ) $offset = $offset - $limit;
        empty($offset) || $query = $query->skip(intval($offset));
        empty($limit) || $query = $query->limit(intval($limit));
        $self = $query->get();


        return ['total' => $total, 'row' => $self];
    }

}
