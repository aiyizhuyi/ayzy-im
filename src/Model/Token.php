<?php

namespace Ayzy\IM\Model;

class Token extends Model
{
    protected $table      = 'token';
    protected $collection = 'token';
    protected $fillable   = [];

    static public function add( $data )
    {
        extract($data);
        $self             = new self();
        $self->username   = $username;
        $self->token      = $token;
        $self->timestamp  = $time ?? time();
        $self->expires_in = $expires_in;
        $self->save();

        return $self->_id;
    }
}