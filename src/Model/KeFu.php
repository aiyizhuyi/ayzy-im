<?php

namespace Ayzy\IM\Model;

class KeFu extends Model
{
    protected $table      = 'kefu';
    protected $collection = 'kefu';
    protected $fillable   = [];

    /**
     * @param $data
     * @return mixed
     * @note  : 添加客服
     * @author: gzh
     * @time  : Times
     */
    static public function add( $data )
    {
        extract($data);
        $self            = new self();
        $self->id        = $id;
        $self->number    = $number;
        $self->timestamp = $timestamp ?? time();
        $self->save();

        return $self->_id;
    }
}