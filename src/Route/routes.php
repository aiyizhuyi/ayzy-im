<?php

Route::group(['prefix' => 'im', 'namespace' => 'Ayzy\IM\App', 'middleware' => ['web', 'auth', 'im']], function( $route ) {
    $route->get('/test/test', 'TestController@index');
    $route->get('/chat', 'ChatController@index'); // 聊天首页
    $route->post('/chat/config', 'ChatController@config'); // 获取配置文件
    $route->post('/chat/save', 'ChatController@save'); // 保存聊天记录
    $route->post('/chat/getUser', 'ChatController@getUser'); // 获取用户信息
    $route->post('/chat/getMessage', 'ChatController@getMessage'); // 获取聊天信息
    $route->post('/chat/isLogin', 'ChatController@isLogin');
});

Route::group(['prefix' => 'im', 'namespace' => 'Ayzy\IM\App'], function( $route ) {
    $route->post('/chat/upload', 'ChatController@upload')->name('chatUpload'); // 上传文件
    $route->post('/chat/save', 'ChatController@save'); // 保存聊天记录
    $route->post('/chat/kefu', 'ChatController@editKeFu'); // 客服记录
});