<?php

/**
 * 配置文件
 */
return [
    /*
     * 客服头像
     */
        'customerServiceAvatar' => '/assets/images/kefu.png',
    /*
     * 表情路径
     */
        'expressionURI'         => '/assets/plugins/layim/layui/images/face/',
    /*
     * 载入右侧视图名称
     */
        'assistURI'             => '/admin/test',
    /*
     * 表情解析
     */
        'expressionAr'          => ["[微笑]", "[嘻嘻]", "[哈哈]", "[可爱]", "[可怜]", "[挖鼻]", "[吃惊]", "[害羞]", "[挤眼]", "[闭嘴]", "[鄙视]", "[爱你]", "[泪]", "[偷笑]", "[亲亲]", "[生病]", "[太开心]", "[白眼]", "[右哼哼]", "[左哼哼]", "[嘘]", "[衰]", "[委屈]", "[吐]", "[哈欠]", "[抱抱]", "[怒]", "[疑问]", "[馋嘴]", "[拜拜]", "[思考]", "[汗]", "[困]", "[睡]", "[钱]", "[失望]", "[酷]", "[色]", "[哼]", "[鼓掌]", "[晕]", "[悲伤]", "[抓狂]", "[黑线]", "[阴险]", "[怒骂]", "[互粉]", "[心]", "[伤心]", "[猪头]", "[熊猫]", "[兔子]", "[ok]", "[耶]", "[good]", "[NO]", "[赞]", "[来]", "[弱]", "[草泥马]", "[神马]", "[囧]", "[浮云]", "[给力]", "[围观]", "[威武]", "[奥特曼]", "[礼物]", "[钟]", "[话筒]", "[蜡烛]", "[蛋糕]"],
    /*
     * 消息每页显示数量
     */
        'messageLimit'          => 10,
    /*
     * 星期对应关系
     */
        'week'                  => ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
    /*
     * 消息显示时间间隔时间 （分钟）
     */
        'messageInterval'       => 5,

    /**
     * 环信 IM 前台配置
     */
        'WebIM'                 => [
            /*
             * XMPP server
             */
                'url'                   => 'im-api.easemob.com', // xmppURL
            /*
             * Backend REST API URL
             */
                'apiURL'                => 'https://a1.easemob.com',
            /*
             * Application AppKey
             */
                'appkey'                => '1191180913099997#mingxuan3040',
            /*
             * Whether to use wss
             * @parameter {Boolean} true or false
             */
                'https'                 => false,

            /*
             * isMultiLoginSessions
             * true: A visitor can sign in to multiple webpages and receive messages at all the webpages.
             * false: A visitor can sign in to only one webpage and receive messages at the webpage.
             */
                'isMultiLoginSessions'  => true,
            /*
             * set presence after login
             */
                'isAutoLogin'           => true,
            /*
             * Whether to use window.doQuery()
             * @parameter {Boolean} true or false
             */
                'isWindowSDK'           => false,
            /*
             * isSandBox=true:  xmppURL: 'im-api-sandbox.easemob.com',  apiURL: '//a1-sdb.easemob.com',
             * isSandBox=false: xmppURL: 'im-api.easemob.com',          apiURL: '//a1.easemob.com',
             * @parameter {Boolean} true or false
             */
                'isSandBox'             => false,
            /*
             * Whether to console.log in strophe.log()
             * @parameter {Boolean} true or false
             */
                'isDebug'               => false,
            /*
             * will auto connect the xmpp server autoReconnectNumMax times in background when client is offline.
             * won't auto connect if autoReconnectNumMax=0.
             */
                'autoReconnectNumMax'   => 2,
            /*
             * the interval seconds between each auto reconnectting.
             * works only if autoReconnectMaxNum >= 2.
             */
                'autoReconnectInterval' => 2,
            /*
             * webrtc supports WebKit and https only
             */
                'isWebRTC'              => "(/Firefox /.test(navigator . userAgent) || /WebKit /.test(navigator . userAgent)) && /^https\:$/.test(window . location . protocol)",
            /*
             * after login, send empty message to xmpp server like heartBeat every 45s, to keep the ws connection alive.
             */
                'heartBeatWait'         => 4500,
            /*
             * while http access,use ip directly,instead of ServerName,avoiding DNS problem.
             */
                'isHttpDNS'             => false,

            /*
             * Will show the status of messages in single chat
             * msgStatus: true  show
             * msgStatus: true  hide
             */
                'msgStatus'             => true,

            /*
             * When a message arrived, the receiver send an ack message to the
             * sender, in order to tell the sender the message has delivered.
             * See call back function onReceivedMessage
             */
                'delivery'              => true,

            /*
             * When a message read, the receiver send an ack message to the
             * sender, in order to tell the sender the message has been read.
             * See call back function onReadMessage
             */
                'read'                  => true,

            /*
             * When a message sent or arrived, will save it into the localStorage,
             * true: Store the chat record
             * false: Don't store the chat record
             */
                'saveLocal'             => false,

            /*
             * Will encrypt text message and emoji message
             * {type:'none'}   no encrypt
             * {type:'base64'} encrypt with base64
             * {type:'aes',mode: 'ebc',key: '123456789easemob',iv: '0000000000000000'} encrypt with aes(ebc)
             * {type:'aes',mode: 'cbc',key: '123456789easemob',iv: '0000000000000000'} encrypt with aes(cbc)
             */
                'encrypt'               => [
                        'type' => 'none'
                ]
        ],
];