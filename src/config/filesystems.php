<?php

return [
        'IM' => [
                'driver'     => 'local',
                'root'       => storage_path('app/public/im'),
                'url'        => storage_path('app/public/im'),
                'visibility' => 'public',
        ],
];
