<?php

return [
        'ayzymongodb' => [
                'driver'   => 'mongodb',
                'host'     => env('DB_HOST', '172.16.5.96'),
                'port'     => env('DB_PORT', '27017'),
                'database' => env('DB_DATABASE', 'ayzyTest'),
        ],
];
